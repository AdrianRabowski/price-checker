# price-checker

Every 30 minutes app checks price for particular hotel and particular room. 
If price is lower or higher it sends notification. 
Available at https://tui-price-checker.herokuapp.com

Endpoints documented under https://tui-price-checker.herokuapp.com/swagger-ui/index.html