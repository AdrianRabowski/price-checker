package com.pricechecker.tui.pricechecker.roomdetails;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@Slf4j
@RestController
@RequestMapping(path = "/details")
public class RoomDetailsController {
    private RoomDetailsRepository roomDetailsRepository;

    public RoomDetailsController(@Autowired RoomDetailsRepository roomDetailsRepository) {
        this.roomDetailsRepository = roomDetailsRepository;
    }

    @Operation(summary = "Get room details by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returns hotel room details from database",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = RoomDetails.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Room details not found",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error",
                    content = @Content)})
    @GetMapping("/{id}")
    public RoomDetails getRoomDetailsById(@PathVariable Integer id) {
        log.info("Get room {} details", id);
        Optional<RoomDetails> response = roomDetailsRepository.findById(id);
        return response.orElseThrow(() -> new ResponseStatusException(
                HttpStatus.NOT_FOUND, "Room details not found"
        ));
    }

    @Operation(summary = "Get all room details")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returns all hotel room details from database",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = RoomDetails.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error",
                    content = @Content)})
    @GetMapping()
    public List<RoomDetails> allRooms() {
        log.info("Get all room details");
        return (List<RoomDetails>) roomDetailsRepository.findAll();
    }

    @Operation(summary = "Save new room details")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Room details saved",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = RoomDetails.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error",
                    content = @Content)})
    @PostMapping()
    public RoomDetails save(@RequestBody RoomDetails roomDetails) {
        log.info("Persist new RoomDetail");
        return roomDetailsRepository.save(roomDetails);
    }

    @Operation(summary = "Edit room details by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Room details updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = RoomDetails.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Room details not found",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error",
                    content = @Content)})
    @PutMapping("/{id}")
    public RoomDetails update(@RequestBody RoomDetails roomDetails, @PathVariable Integer id) {
        log.info("Update RoomDetail {}", id);
        if (roomDetailsRepository.existsById(id)) {
            RoomDetails detailsToUpdate = getRoomDetailsToUpdate(roomDetails, id);
            return roomDetailsRepository.save(detailsToUpdate);
        } else {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Room details not found");
        }
    }

    private RoomDetails getRoomDetailsToUpdate(RoomDetails roomDetails, int id) {
        return RoomDetails.builder().details(roomDetails.getDetails())
                        .departureDate(roomDetails.getDepartureDate())
                        .returnDate(roomDetails.getReturnDate())
                        .id(id)
                        .airportName(roomDetails.getAirportName())
                        .roomCode(roomDetails.getRoomCode())
                        .roomName(roomDetails.getRoomName())
                        .airportName(roomDetails.getAirportName())
                        .emails(roomDetails.getEmails())
                        .discountPrice(roomDetails.getDiscountPrice())
                        .duration(roomDetails.getDuration())
                        .offerCode(roomDetails.getOfferCode())
                        .originalPrice(roomDetails.getOriginalPrice())
                        .receivedOn(roomDetails.getReceivedOn())
                        .build();
    }

    @Operation(summary = "Delete room details by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Room details deleted"),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Room details not found",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error",
                    content = @Content)})
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        log.info("Deleting RoomDetail {}", id);
        if (roomDetailsRepository.existsById(id)) {
            roomDetailsRepository.deleteById(id);
        } else {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Room details not found");
        }
    }

}
